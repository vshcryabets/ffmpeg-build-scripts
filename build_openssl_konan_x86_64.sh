#!/bin/bash
. settings_konan_x86_64.sh

unset AR
unset AS
unset LD
unset RANLIB
unset CC
unset NM

cd openssl

make clean

perl -pi -e 's/install: all install_docs install_sw/install: install_docs install_sw/g' Makefile.org
./Configure linux-x86_64 --prefix=$ANDROID_PREFIX && make depend && make all && make install

cd --
