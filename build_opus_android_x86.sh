#!/bin/bash
. settings_x86.sh

cd opus
./configure \
    --prefix=$ANDROID_PREFIX \
    --exec-prefix=$ANDROID_PREFIX \
    --target=${CROSS_COMPILE} \
    --enable-shared \
    --enable-static \
    --enable-pic \
    --with-ssl=$ANDROID_PREFIX \
    --enable-strip \
    --host=${CROSS_COMPILE} \
    $ANDROID_ADDITIONAL_CONFIGURE_FLAG \
    && make clean \
    && make \
    && make install

cd --
