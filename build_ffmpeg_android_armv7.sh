#!/bin/bash
. settings_armv7.sh

cd ffmpeg

./configure \
    --prefix=$ANDROID_PREFIX \
    --enable-shared \
    --enable-decoder=h264 \
    --enable-encoder=h264 \
    --enable-decoder=aac \
    --enable-encoder=aac \
    --enable-parser=h264 \
    --enable-libx264 \
    --enable-static \
    --disable-doc \
    --enable-ffmpeg \
    --enable-ffplay \
    --disable-ffprobe \
    --disable-ffserver \
    --disable-avdevice \
    --enable-gpl \
    --disable-doc \
    --disable-symver \
    --extra-cflags="$CPPFLAGS" \
    --extra-ldflags="$ADDI_LDFLAGS -L$PREFIX/lib" \
    --extra-libs=-ldl \
    --sysroot=$ANDROID_SYSROOT \
    --cross-prefix=$CROSS_COMPILE- \
    --target-os=$CROSS_COMPILE \
    --target_arch=$ANDROID_CPU \
    --enable-cross-compile \
    $ADDITIONAL_CONFIGURE_FLAG
make clean
make
make install
cd --
