#!/bin/bash
. settings.sh

PREFIX=$(pwd)/build/android/$ANDROID_CPU 
SYSROOT=$NDK/platforms/$PLATFORM/arch-arm/
TOOLCHAIN=$NDK/toolchains/$TOOLCHAIN/prebuilt/$HOST_OS

function build_one
{
cd lame
./configure \
    --prefix=$PREFIX \
        --enable-shared \
        --enable-static \
        --disable-opencl \
        --enable-pic \
        --enable-strip \
        --disable-nasm \
        --cross-prefix=$TOOLCHAIN/bin/arm-linux-androideabi- \
        --host=arm-linux \
        --sysroot=$SYSROOT \
        $ANDROID_ADDITIONAL_CONFIGURE_FLAG
    make clean
    make
    make install
cd --
}

build_one