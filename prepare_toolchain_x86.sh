#!/bin/bash

. settings_x86.sh

rm -rf $ANDROID_TOOLCHAIN

$NDK/build/tools/make_standalone_toolchain.py \
    --arch x86 \
    --api $PLATFORMINT\
    --stl libc++ \
    --install-dir $ANDROID_TOOLCHAIN

