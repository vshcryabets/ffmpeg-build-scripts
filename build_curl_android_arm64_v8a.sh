#!/bin/bash
. settings_arm64_v8a.sh

cd curl
./configure \
    --prefix=$ANDROID_PREFIX \
    --exec-prefix=$ANDROID_PREFIX \
    --target=${CROSS_COMPILE} \
    --enable-shared \
    --enable-static \
    --with-ssl=$ANDROID_PREFIX \
    --enable-pic \
    --enable-strip \
    --host=${CROSS_COMPILE} \
    $ANDROID_ADDITIONAL_CONFIGURE_FLAG \
    && make clean \
    && make \
    && make install

cd --
