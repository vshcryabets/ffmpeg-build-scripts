#!/bin/bash
source settings_x86_64.sh

cd libpng
./configure \
    --prefix=$ANDROID_PREFIX \
    --exec-prefix=$ANDROID_PREFIX \
    --includedir=$ANDROID_PREFIX/include \
    --target=${CROSS_COMPILE} \
    --enable-shared \
    --enable-static \
    --enable-pic \
    --with-ssl=$ANDROID_PREFIX \
    --enable-strip \
    --host=${CROSS_COMPILE} \
    $ANDROID_ADDITIONAL_CONFIGURE_FLAG && \
	make clean && \
	make && \
	make install

cd --
