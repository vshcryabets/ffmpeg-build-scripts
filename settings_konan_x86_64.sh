#!/bin/bash

export CROSS_COMPILE="x86_64-unknown-linux-gnu-"
ANDROID_CPU=x86_64-konan

source ./settings.sh

ANDROID_TOOLCHAIN=$(pwd)/konan/
ANDROID_SYSROOT=$ANDROID_TOOLCHAIN/sysroot/
ANDROID_PREFIX=$(pwd)/build/konan/$ANDROID_CPU
export PATH=$ANDROID_TOOLCHAIN/bin:$PATH

#export CPPFLAGS="-Os -fpic $ANDROID_ADDI_CFLAGS -I$PREFIX/include -std=c99"
export AR=${CROSS_COMPILE}ar
export AS=${CROSS_COMPILE}as
export LD=${CROSS_COMPILE}ld
export RANLIB=${CROSS_COMPILE}ranlib
export CC=${CROSS_COMPILE}gcc
export NM=${CROSS_COMPILE}nm
export CPP="${CROSS_COMPILE}gcc -E"
export CXX=${CROSS_COMPILE}g++
