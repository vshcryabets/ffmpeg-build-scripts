#!/bin/bash

. settings_x86_64.sh

rm -rf $ANDROID_TOOLCHAIN

$NDK/build/tools/make_standalone_toolchain.py \
    --arch x86_64 \
    --api $PLATFORMINT\
    --stl libc++ \
    --install-dir $ANDROID_TOOLCHAIN
