#!/bin/bash
. settings_armv7.sh

cd x264
./configure \
    --prefix=$ANDROID_PREFIX \
    --enable-shared \
    --enable-static \
    --disable-opencl \
    --enable-pic \
    --enable-strip \
    --extra-ldflags="$ANDROID_ADDI_LDFLAGS" \
    --cross-prefix=$CROSS_COMPILE- \
    --host=arm-linux \
    --sysroot=$ANDROID_SYSROOT \
    $ANDROID_ADDITIONAL_CONFIGURE_FLAG && make clean && make AS=$CC && make install
cd --
