#!/bin/bash
. settings.sh

PREFIX=$(pwd)/build/host/$CPU 

function build_one
{
    cd ffmpeg
    ./configure \
        --prefix=$PREFIX \
        --enable-shared \
        --enable-decoder=h264 \
        --enable-encoder=h264 \
        --enable-decoder=aac \
        --enable-encoder=aac \
        --enable-parser=h264 \
        --enable-libx264 \
        --enable-static \
        --disable-doc \
        --enable-ffmpeg \
        --enable-ffplay \
        --disable-ffprobe \
        --disable-ffserver \
        --disable-avdevice \
        --enable-gpl \
        --disable-doc \
        --disable-symver \
        --extra-cflags="-Os -fpic $ADDI_CFLAGS -I$PREFIX/include" \
        --extra-ldflags="$ADDI_LDFLAGS -L$PREFIX/lib" \
        --extra-libs=-ldl \
        $ADDITIONAL_CONFIGURE_FLAG
    make clean
    make
    make install
    cd -
}

build_one