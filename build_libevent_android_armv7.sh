#!/bin/bash
. settings_armv7.sh

cd libevent
./configure \
    --prefix=$ANDROID_PREFIX \
    --exec-prefix=$ANDROID_PREFIX \
    --target=${CROSS_COMPILE} \
    --enable-shared \
    --enable-static \
    --enable-pic \
    --disable-debug-mode \
    --disable-openssl \
    --enable-strip \
    --host=${CROSS_COMPILE} \
    _XOPEN_SOURCE=700 \
    $ANDROID_ADDITIONAL_CONFIGURE_FLAG \
    && make clean \
    && make \
    && make install

cd --
