#!/bin/bash
. settings.sh

PREFIX=$(pwd)/build/android/$ANDROID_CPU 
SYSROOT=$NDK/platforms/$PLATFORM/arch-arm/
TOOLCHAIN=$NDK/toolchains/$TOOLCHAIN/prebuilt/$HOST_OS

function build_one
{
cd vo-aacenc
./configure \
    --prefix=$PREFIX \
        --enable-armv7neon \
        --enable-shared \
        --enable-static \
        --disable-opencl \
        --enable-pic \
        --enable-strip \
        --disable-asm \
        --extra-cflags="-Os -fpic $ANDROID_ADDI_CFLAGS" \
        --extra-ldflags="$ANDROID_ADDI_LDFLAGS" \
        --cross-prefix=$TOOLCHAIN/bin/arm-linux-androideabi- \
        --host=arm-linux \
        --sysroot=$SYSROOT \
        $ANDROID_ADDITIONAL_CONFIGURE_FLAG
    make clean
    make
    make install
cd --
}

build_one