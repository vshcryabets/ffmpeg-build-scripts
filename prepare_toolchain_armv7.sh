#!/bin/bash

. settings_armv7.sh

rm -rf $ANDROID_TOOLCHAIN

$NDK/build/tools/make_standalone_toolchain.py \
    --arch arm \
    --api $PLATFORMINT\
    --stl libc++ \
    --install-dir $ANDROID_TOOLCHAIN

