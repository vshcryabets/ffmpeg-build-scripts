#!/bin/bash
. settings_armv5.sh

cd crypto++
rm -r Makefile
mv GNUmakefile GNUmakefile.host
cp GNUmakefile-cross Makefile

export PREFIX=$ANDROID_PREFIX
export IS_ANDROID=1
export ANDROID_FLAGS=$ANDROID_ADDI_FLAGS
export ANDROID_STL_INC=$ANDROID_SYSROOT/usr/include/
make clean 
make
make install

cd --
