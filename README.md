Steps:
1. Edit settings.sh

2. Run prepare_toolchain.sh. This script will prepare standalone NDK toolchian.

3. Clone libx264:
git clone git://git.videolan.org/x264.git x264

4. Clone ffmpeg
git clone --branch release/2.3 https://github.com/FFmpeg/FFmpeg.git ffmpeg