#!/bin/bash
. settings_arm64_v8a.sh

cd zlib
./configure \
    --prefix=$ANDROID_PREFIX \
    --eprefix=$ANDROID_PREFIX \
    $ANDROID_ADDITIONAL_CONFIGURE_FLAG \
    && make clean \
    && make \
    && make install

cd --
