#!/bin/bash

cd openssl
make clean
./Configure darwin64-x86_64-cc --openssldir=../build/host && make clean && make depend && make all && make install

cd --
