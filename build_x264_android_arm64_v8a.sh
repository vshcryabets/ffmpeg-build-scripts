#!/bin/bash
. settings_arm64_v8a.sh

function build_one
{
    cd x264
    ./configure \
        --prefix=$ANDROID_PREFIX \
        --enable-shared \
        --enable-static \
        --disable-opencl \
        --enable-pic \
        --enable-strip \
        --disable-asm \
        --extra-cflags="-Os -fpic $ANDROID_ADDI_CFLAGS" \
        --extra-ldflags="$ANDROID_ADDI_LDFLAGS" \
        --cross-prefix=$CROSS_COMPILE- \
        --host=arm-linux \
        --sysroot=$ANDROID_SYSROOT \
        $ANDROID_ADDITIONAL_CONFIGURE_FLAG && make clean && make && make install
cd --
}

build_one