#!/bin/bash

build () {
	cd libjpeg-turbo
	make clean
#	autoreconf -fiv
	./configure \
	    --prefix=$ANDROID_PREFIX \
	    --exec-prefix=$ANDROID_PREFIX \
	    --target=${CROSS_COMPILE} \
	    --enable-static \
	    --enable-pic \
	    --enable-strip \
	    --host=${CROSS_COMPILE} \
	    --with-zlib=yes \
	    --with-bzip2=no \
	    --with-png=no \
	    --with-brotli=no \
	    --with-harfbuzz=no \
	    --with-old-mac-fonts=no \
	    --with-fsspec=no \
	    --with-fsref=no \
	    --with-quickdraw-toolbox=no \
	    --with-quickdraw-carbon=no \
	    --with-ats=no \
	    $ANDROID_ADDITIONAL_CONFIGURE_FLAG
	make && make install
	cd ..
}

. settings_arm64_v8a.sh
build

. settings_armv7.sh
build

. settings_x86_64.sh
build

. settings_x86.sh
build

