#!/bin/bash
. settings_x86.sh

cd ffmpeg

#    --enable-decoder=h264 \
#    --enable-encoder=h264 \
#    --enable-decoder=aac \
#    --enable-encoder=aac \
#    --enable-parser=h264 \
#    --enable-libx264 \

./configure \
    --target-os=linux \
    --arch=x86 \
    --cpu=i686 \
    --cross-prefix=$CROSS_COMPILE- \
    --enable-cross-compile \
    --enable-shared \
    --enable-gpl \
    --enable-static \
    --disable-symver \
    --disable-doc \
    --enable-ffmpeg \
    --enable-ffplay \
    --disable-ffprobe \
    --disable-ffserver \
    --disable-avdevice \
    --disable-postproc \
    --disable-encoders \
    --disable-muxers \
    --disable-devices \
    --disable-demuxer=sbg \
    --disable-demuxer=dts \
    --disable-parser=dca \
    --disable-decoder=dca \
    --disable-decoder=svq3 \
    --enable-network \
    --enable-version3 \
    --disable-amd3dnow \
    --disable-amd3dnowext \
    --enable-asm \
    --enable-yasm \
    --enable-pic \
    --prefix=$ANDROID_PREFIX \
    --extra-cflags="$CPPFLAGS" \
    --extra-ldflags="$ANDROID_ADDI_LDFLAGS -L$PREFIX/lib" \
    --extra-libs=-ldl \
    $ADDITIONAL_CONFIGURE_FLAG
    
if [ $? -ne 0 ]; then
    echo "Configuration error"
    exit $?
fi

make clean
make
make install

cd --
