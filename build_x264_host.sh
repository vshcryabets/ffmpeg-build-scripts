#!/bin/bash
. settings.sh

PREFIX=$(pwd)/build/host/

function build_one
{
  cd x264
  ./configure \
    --prefix=$PREFIX \
        --enable-shared \
        --enable-static \
        --disable-opencl \
        --enable-pic \
        --enable-strip \
        --extra-cflags="-Os -fpic $ADDI_CFLAGS" \
        --extra-ldflags="$ADDI_LDFLAGS" \
        $ADDITIONAL_CONFIGURE_FLAG
    make clean
    make
    make install
    cd -
}

build_one