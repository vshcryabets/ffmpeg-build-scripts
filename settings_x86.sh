#!/bin/bash

export CROSS_COMPILE="i686-linux-android"
export ANDROID_CPU=x86

source ./settings.sh

HOST_OS=darwin-x86_64
ANDROID_ADDI_CFLAGS="-pipe -DANDROID -DNDEBUG -march=atom -msse3 -ffast-math -mfpmath=sse"

export CPPFLAGS="-Os -fpic $ANDROID_ADDI_CFLAGS -I$PREFIX/include -std=c99"
export AR=${CROSS_COMPILE}-ar
export AS=${CROSS_COMPILE}-as
export LD=${CROSS_COMPILE}-ld
export RANLIB=${CROSS_COMPILE}-ranlib
export CC=${CROSS_COMPILE}-gcc
export NM=${CROSS_COMPILE}-nm
export CPP="${CROSS_COMPILE}-gcc -E"
export CXX=${CROSS_COMPILE}-g++
export OBJDUMP=${CROSS_COMPILE}-objdump
export STRIP=${CROSS_COMPILE}-strip
