#!/bin/bash
. settings_arm64_v8a.sh

cd opus
./configure \
    --prefix=$ANDROID_PREFIX \
    --exec-prefix=$ANDROID_PREFIX \
    --target=${CROSS_COMPILE} \
    --enable-shared \
    --enable-fixed-point \
    --enable-static \
    --with-ssl=$ANDROID_PREFIX \
    --enable-pic \
    --enable-strip \
    --disable-extra-programs \
    --disable-doc \
    --host=${CROSS_COMPILE} \
    $ANDROID_ADDITIONAL_CONFIGURE_FLAG \
    && make clean \
    && make \
    && make install

cd --
