#!/bin/bash

export CROSS_COMPILE="arm-linux-androideabi"
export ANDROID_CPU=armv7

source ./settings.sh

HOST_OS=darwin-x86_64
ANDROID_ADDI_CFLAGS="-mfpu=neon -marm -march=armv7-a -mtune=cortex-a8"

export CPPFLAGS="-Os -fPIC $ANDROID_ADDI_CFLAGS -pie"
export AR=${CROSS_COMPILE}-ar
export AS=${CROSS_COMPILE}-as
export LD=${CROSS_COMPILE}-ld
export RANLIB=${CROSS_COMPILE}-ranlib
export NM=${CROSS_COMPILE}-nm
export CPP="${CROSS_COMPILE}-gcc -E"
export CXX=${CROSS_COMPILE}-g++
export CC=${CROSS_COMPILE}-gcc
export OBJDUMP=${CROSS_COMPILE}-objdump
export STRIP=${CROSS_COMPILE}-strip
