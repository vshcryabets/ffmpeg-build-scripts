#!/bin/bash
. settings_arm64_v8a.sh

cd opencv
rm -rf build
mkdir build
cd build

export ANDROID_NDK=$NDK
cmake \
    -DANDROID_TOOLCHAIN_NAME=standalone \
    -DANDROID_STANDALONE_TOOLCHAIN=$ANDROID_TOOLCHAIN \
    -DANDROID_STL_FORCE_FEATURES=ON \
    -DANDROID_STL=c++_static \
    -DANDROID_ABI="arm64-v8a" \
    -DANDROID_NATIVE_API_LEVEL=$PLATFORM \
    -DBUILD_ANDROID_EXAMPLES=OFF \
    -DBUILD_DOCS=OFF \
    -DBUILD_FAT_JAVA_LIB=OFF \
    -DBUILD_JASPER=OFF \
    -DBUILD_OPENEXR=OFF \
    -DBUILD_CAROTENE=OFF \
    -DBUILD_PNG=OFF \
    -DBUILD_opencv_gpu=OFF \
    -DBUILD_PACKAGE=OFF \
    -DBUILD_PERF_TESTS=OFF \
    -DBUILD_TESTS=OFF \
    -DBUILD_TIFF=OFF \
    -DBUILD_WITH_DEBUG_INFO=OFF \
    -DBUILD_opencv_apps=OFF \
    -DBUILD_opencv_java=OFF \
    -DBUILD_opencv_python2=OFF \
    -DBUILD_opencv_world=OFF \
    -DCMAKE_C_FLAGS_RELEASE="-Os -DNDEBUG -fvisibility=hidden -ffunction-sections -fstack-protector-all" \
    -DCMAKE_CXX_FLAGS_RELEASE="-Os -DNDEBUG -fvisibility=hidden -ffunction-sections -fstack-protector-all -fvisibility-inlines-hidden" \
    -DENABLE_PRECOMPILED_HEADERS=OFF \
    -DWITH_EIGEN=OFF \
    -DWITH_JASPER=OFF \
    -DWITH_OPENEXR=OFF \
    -DWITH_TIFF=OFF \
    -DWITH_TBB=OFF \
    -DWITH_CAROTENE=OFF \
    -DWITH_CUDA=OFF \
    -DWITH_CUFFT=OFF -DWITH_WEBP=OFF \
    -DCMAKE_BUILD_WITH_INSTALL_RPATH=ON \
    -DCMAKE_TOOLCHAIN_FILE=./platforms/android/android.toolchain.cmake ..

make
cd --
