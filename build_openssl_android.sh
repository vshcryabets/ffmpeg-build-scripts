#!/bin/bash

build () {

	unset AR
	unset AS
	unset LD
	unset RANLIB
	unset CC
	unset NM

	cd openssl
	make clean
	perl -pi -e 's/install: all install_docs install_sw/install: install_docs install_sw/g' Makefile.org

	./Configure $1 no-asm --prefix=$ANDROID_PREFIX && \
    	make depend && \
    	make all && \
    	make install
	cd ..
}

. settings_arm64_v8a.sh
build "android-arm64"

. settings_armv7.sh
build "android-arm"

source settings_x86_64.sh
build "android-x86_64"

. settings_x86.sh
build "android-x86"

