#!/bin/bash

. settings_arm64_v8a.sh

rm -rf $ANDROID_TOOLCHAIN

$NDK/build/tools/make_standalone_toolchain.py \
    --arch arm64 \
    --api $PLATFORMINT\
    --stl libc++ \
    --install-dir $ANDROID_TOOLCHAIN

