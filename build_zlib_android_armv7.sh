#!/bin/bash
. settings_armv7.sh

cd zlib
./configure \
    --prefix=$ANDROID_PREFIX \
    --eprefix=$ANDROID_PREFIX \
    $ANDROID_ADDITIONAL_CONFIGURE_FLAG \
    && make clean \
    && make \
    && make install

cd --
