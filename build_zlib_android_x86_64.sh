#!/bin/bash
source settings_x86_64.sh

cd zlib
./configure \
    --prefix=$ANDROID_PREFIX \
    --static \
    --eprefix=$ANDROID_PREFIX \
    $ANDROID_ADDITIONAL_CONFIGURE_FLAG &&\
	make clean && \
	make && \
	make install

cd --
