#!/bin/bash

export CROSS_COMPILE="aarch64-linux-android"
export ANDROID_CPU=arm64_v8a

source ./settings.sh

ANDROID_ARCH=arm64
ANDROID_ADDI_CFLAGS="-march=armv8-a"

ANDROID_INCLUDES="-I${SYSROOT}/usr/include -I${TOOLCHAIN}/include"

export CPPFLAGS="-Os -fPIC $ANDROID_ADDI_CFLAGS -pie"
export AR=${CROSS_COMPILE}-ar
export AS=${CROSS_COMPILE}-as
export LD=${CROSS_COMPILE}-ld
export RANLIB=${CROSS_COMPILE}-ranlib
export CC=${CROSS_COMPILE}-gcc
export NM=${CROSS_COMPILE}-nm
export CPP="${CROSS_COMPILE}-gcc -E"
export CXX=${CROSS_COMPILE}-g++
export OBJDUMP=${CROSS_COMPILE}-objdump
export STRIP=${CROSS_COMPILE}-strip
